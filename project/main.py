"""Blackjack"""

import random

from art import LOGO

############### Blackjack Project #####################

# Difficulty Normal: Use all Hints below to complete the project.
# Difficulty Hard: Use only Hints 1, 2, 3 to complete the project.
# Difficulty Extra Hard: Only use Hints 1 & 2 to complete the project.
# Difficulty Expert: Only use Hint 1 to complete the project.

############### Our Blackjack House Rules #####################

## The deck is unlimited in size.
## There are no jokers.
## The Jack/Queen/King all count as 10.
## The the Ace can count as 11 or 1.
## Use the following list as the deck of cards:
## cards = [11, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10]
## The cards in the list have equal probability of being drawn.
## Cards are not removed from the deck as they are drawn.
## The computer is the dealer.

##################### Hints #####################

# Hint 1: Go to this website and try out the Blackjack game:
#   https://games.washingtonpost.com/games/blackjack/
# Then try out the completed Blackjack project here:
#   http://blackjack-final.appbrewery.repl.run

play = "y"


def get_card():
    """Returns a random card value"""
    CARDS = [11, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10]
    return random.choice(CARDS)


def deal_card(hand):
    """Deals a new card and updates the score"""
    card = get_card()
    score = hand["score"] + card
    hand["cards"].append(card)

    if card == 11:
        hand["aces"] += 1

    if score > 21 and hand["aces"] > 0:
        hand["score"] = score - 10
        hand["aces"] -= 1
    else:
        hand["score"] = score


def end_game():
    """Function to pick the winner"""

    print(f'   Your final hand: {mycards["cards"]}, final score: {mycards["score"]}')
    print(
        f'   Computer\'s final hand: {computercards["cards"]}, final score: {computercards["score"]}'
    )

    if mycards["score"] == 21 and len(mycards["cards"]) == 2:
        print("Blackjack!")
    elif computercards["score"] == 21 and len(computercards["cards"]) == 2:
        print("Blackjack!")

    if mycards["score"] == computercards["score"]:
        print("It's a draw.")
    elif mycards["score"] > 21 and computercards["score"] > 21:
        print("It's a draw.")
    elif mycards["score"] > 21:
        print("You went over. You lose.")
    elif mycards["score"] < computercards["score"] and computercards["score"] < 22:
        print("You lose.")
    else:
        print("You win!")


while play == "y":
    mycards = {"score": 0, "aces": 0, "cards": []}
    computercards = {"score": 0, "aces": 0, "cards": []}
    another = "y"

    print(LOGO)

    deal_card(mycards)
    deal_card(computercards)
    deal_card(computercards)

    while another == "y":
        deal_card(mycards)

        print(f'   Your cards: {mycards["cards"]}, current score: {mycards["score"]}')
        print(f'   Computer\'s first card: {computercards["cards"][0]}')

        if mycards["score"] < 21 and computercards["score"] < 21:
            another = input("Type 'y' to get another card, type 'n' to pass: ").lower()
        else:
            another = "n"

    while computercards["score"] < 17:
        deal_card(computercards)

    end_game()

    play = input("Do you want to play a game of Blackjack? Type 'y' or 'n': ").lower()
